﻿using JWT;
using JWT.Algorithms;
using JWT.Builder;
using NewTransitionsAPI.Models;
using NewTransitionsAPI.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NewTransitionsAPI.Controllers
{
	public class BackgroundPickerController : ApiController
	{
		private BackgroundPickerContext db = new BackgroundPickerContext();

		public List<BackgroundPickerViewModel> GetFullNames()
		{

			var customers = db.GetFullNames.AsEnumerable().Select(x => new BackgroundPickerViewModel
			{
				Id = x.Id,
				FullName = x.FullName,
				Background = x.Background,
				DateCreated = x.DateCreated


			}).ToList<BackgroundPickerViewModel>();

			return customers;
		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		public HttpResponseMessage Options()
		{
			var response = new HttpResponseMessage();
			response.StatusCode = HttpStatusCode.OK;
			return response;
		}


		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/backgroundpicker/postfullname")]
		[HttpPost]
		[AcceptVerbs("OPTIONS")]
		public async Task<IHttpActionResult> PostFullName(BackgroundPickerViewModel customer)
		{
			try
			{
				var cust = new BackgroundPicker
				{
					FullName = customer.FullName,
					Background = customer.Background,
					DateCreated = customer.DateCreated
				};

				db.GetFullNames.Add(cust);
				db.SaveChanges();
				const string secret = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";
				var token = new JwtBuilder()
				  .WithAlgorithm(new HMACSHA256Algorithm())
				  .WithSecret(secret)
				  .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
				  .AddClaim("claim2", "claim2-value")
				  .Build();
				//HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Successfully Save");
				var response = new BackgroundPickerResponse();
				response.Status = "Successfully Save";
				//response.ResultId = Guid.NewGuid();
				//response.Answer = "";
				response.ErrorMessage = token;

				try
				{
					var json = new JwtBuilder()
						.WithSecret(secret)
						.MustVerifySignature()
						.Decode(token);
					Console.WriteLine(json);
				}
				catch (TokenExpiredException)
				{
					Console.WriteLine("Token has expired");
				}
				catch (SignatureVerificationException)
				{
					Console.WriteLine("Token has invalid signature");
				}

				return Ok(response);
			}
			catch (Exception ex)
			{

				HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
				return Ok("Null Value");
			}


		}

	}
}
