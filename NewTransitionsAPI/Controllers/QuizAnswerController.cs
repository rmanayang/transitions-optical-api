﻿using Newtonsoft.Json;
using NewTransitionsAPI.Models;
using NewTransitionsAPI.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NewTransitionsAPI.Controllers
{
	public class QuizAnswerController: ApiController
	{
		private NewTransitionsContext db = new NewTransitionsContext();

		public List<QuizAnswerViewModel> GetQuizAnswer()
		{

			var customers = db.GetQuizAnswer.AsEnumerable().Select(x => new QuizAnswerViewModel
			{

				Id = x.Id,
				Answers = new AnswerViewModel(x.Answer1, x.Answer2, x.Answer3, x.Answer4, x.Answer5, x.Answer6), 
				ECPId = x.ECPId,
				ResultId = x.ResultId,
				DateCreated = x.DateCreated,
				Result = x.Result,
				Demographics = new DemographicsModel(x.eyeglasses_frequency, x.gender, x.age_group, x.Id)
				 

			}).ToList<QuizAnswerViewModel>();
			 

			return customers;
		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		public HttpResponseMessage Options()
		{
			var response = new HttpResponseMessage();
			response.StatusCode = HttpStatusCode.OK;
			return response;
		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/quiz/lifestyle")]
		[HttpGet]
		public List<LifeStyleDetail> GetLifeStyle()
		{
			var customers = db.GetLifeStyleDetail.AsEnumerable().Select(x => new LifeStyleDetail
			{

				LifeStyleId = x.LifeStyleId,
				lifestyle = x.lifestyle


			}).ToList<LifeStyleDetail>();


			return customers;
		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/quiz/answer")]
		[HttpPost]
		[AcceptVerbs("OPTIONS")]
		public async Task<IHttpActionResult> PostQuizAnswer(QuizAnswerViewModel quizanswer)
		{
			try
			{ 
				Guid guid = Guid.NewGuid();
				string _sensitivity = "";
				int total = Convert.ToInt32(quizanswer.Answers.Answer1) + Convert.ToInt32(quizanswer.Answers.Answer2) + Convert.ToInt32(quizanswer.Answers.Answer3) + Convert.ToInt32(quizanswer.Answers.Answer4) + Convert.ToInt32(quizanswer.Answers.Answer5) + Convert.ToInt32(quizanswer.Answers.Answer6);
				if (total <= 6 && total >= 5)
					_sensitivity = "high";
				else if (total <= 4 && total >= 3)
					_sensitivity = "medium";
				else if (total <= 2 && total >= 1)
					_sensitivity = "low";
				else if (total <= 0)
					_sensitivity = "none";

				var id = 0;
				var quizans = new QuizAnswer
				{ 
					Answer1 = quizanswer.Answers.Answer1,
					Answer2 = quizanswer.Answers.Answer2,
					Answer3 = quizanswer.Answers.Answer3,
					Answer4 = quizanswer.Answers.Answer4,
					Answer5 = quizanswer.Answers.Answer5,
					Answer6 = quizanswer.Answers.Answer6,
					ECPId = quizanswer.ECPId,
					ResultId = guid,
					Result = _sensitivity,
					DateCreated = DateTime.Now,
					eyeglasses_frequency = quizanswer.Demographics != null ? quizanswer.Demographics.eyeglasses_frequency : null,
					gender = quizanswer.Demographics != null ?  quizanswer.Demographics.gender : null,
					age_group = quizanswer.Demographics != null ? quizanswer.Demographics.age_group : null
				};

				db.GetQuizAnswer.Add(quizans);
				db.SaveChanges();
				id = quizans.Id;

				if(quizanswer.Demographics != null)
					foreach (string item in quizanswer.Demographics.LifeStyle)
					{
						var _lifestyle = new LifeStyleDetail
						{
							lifestyle = item,
							QuizAnswerId = id 
						};

						db.GetLifeStyleDetail.Add(_lifestyle);
					}

				db.SaveChanges();

				//HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Successfully Save");
				var response = new BackgroundPickerResponse();
				response.Status = "Successfully Save";
				AnswerData _AnswerData = new AnswerData();
				_AnswerData.ResultId = guid;
				AnswerViewModel _AnswerViewModel = new AnswerViewModel();
				_AnswerViewModel.Answer1 = quizanswer.Answers.Answer1;
				_AnswerViewModel.Answer2 = quizanswer.Answers.Answer2;
				_AnswerViewModel.Answer3 = quizanswer.Answers.Answer3;
				_AnswerViewModel.Answer4 = quizanswer.Answers.Answer4;
				_AnswerViewModel.Answer5 = quizanswer.Answers.Answer5;
				_AnswerViewModel.Answer6 = quizanswer.Answers.Answer6;
				_AnswerData.Answers = _AnswerViewModel;
				
				_AnswerData.Sensitivity = _sensitivity;

				_AnswerData.Demographics = quizanswer.Demographics;
				response.AnswerData = _AnswerData;
				response.ErrorMessage = null;
				return Ok(response);
			}
			catch (Exception ex)
			{

				var response = new BackgroundPickerResponse();
				response.Status = "Error";
				response.AnswerData = null; 
				response.ErrorMessage = ex.Message;
				//return Ok(response);
				return StatusCode(HttpStatusCode.BadRequest);
			}


		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/quiz/stats/activity")]
		[HttpGet]
		public object GetActivities(string ecp_id = "", string age_group = "", string gender = "", string eyeglasses_frequency = "")
		{
			if (!string.IsNullOrEmpty(age_group))
				if (age_group.Length > 1)
					if (age_group.Substring(0, 2) == "60")
						age_group = "60+";
			var answers = db.GetQuizAnswer.AsEnumerable().Where(x => x.ECPId == (string.IsNullOrEmpty(ecp_id) ? x.ECPId : ecp_id)
			                                                   && x.age_group == (string.IsNullOrEmpty(age_group) ? x.age_group : age_group)
															   && x.gender == (string.IsNullOrEmpty(gender) ? x.gender : gender)
															   && x.eyeglasses_frequency == (string.IsNullOrEmpty(eyeglasses_frequency) ? x.eyeglasses_frequency : eyeglasses_frequency)).Select(x => new QuizAnswer
			{

				Id = x.Id,
				DateCreated = x.DateCreated


			}).OrderBy(c => c.DateCreated).ToList<QuizAnswer>();

			//ActivityViewModel _temp = new ActivityViewModel();

			//_temp.date1 = 1;
			//_temp.date2 = 2;


			//List<Dictionary<string, int>> _temp = new List<Dictionary<string, int>>();
			//	Dictionary <string, int> _t = new Dictionary<string, int>();
			//_t["date1"] = 1;
			//_temp.Add(_t);
			//_temp.Add(_t);

			Dictionary<string, int> values = new Dictionary<string, int>();
			//values.Add("key1", 1);
			//values.Add("key2", 2);
			//string json = JsonConvert.SerializeObject(values); // { // "key1": "value1", // "key2": "value2" // }

			string _currentDate = "";
			int ctr = 0;
			int count = 0;
			foreach (QuizAnswer answer in answers)
			{
				string _date = ((DateTime)answer.DateCreated).ToString("yyyy-MM-dd");
				if (_date != _currentDate)
				{
					if (ctr == 0)
						ctr++;
					else
					{
						values.Add(_currentDate, ctr);
						ctr = 1;
					}
					_currentDate = _date;
					
				}
				else {
					ctr++;
				}
				count++;
				if(count == answers.Count)
					values.Add(_currentDate, ctr);
			}

			return values;
		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/quiz/stats/sensitivity")]
		[HttpGet]
		public object GetSensitivity(string ecp_id = "", string age_group = "", string gender = "", string eyeglasses_frequency = "")
		{
			if (!string.IsNullOrEmpty(age_group))
				if (age_group.Length > 1)
					if (age_group.Substring(0, 2) == "60")
						age_group = "60+";
			var answers = db.GetQuizAnswer.AsEnumerable().Where(x => x.ECPId == (string.IsNullOrEmpty(ecp_id) ? x.ECPId : ecp_id)
															   && x.age_group == (string.IsNullOrEmpty(age_group) ? x.age_group : age_group)
															   && x.gender == (string.IsNullOrEmpty(gender) ? x.gender : gender)
															   && x.eyeglasses_frequency == (string.IsNullOrEmpty(eyeglasses_frequency) ? x.eyeglasses_frequency : eyeglasses_frequency)).Select(x => new QuizAnswer
			{

				Id = x.Id,
				Result = x.Result


			}).OrderBy(c => c.Result).ToList<QuizAnswer>();

			 
			Dictionary<string, int> values = new Dictionary<string, int>(); 

			string _currentSensitivity = "";
			int ctr = 0;
			int count = 0;
			foreach (QuizAnswer answer in answers)
			{
				string _sensitivity = answer.Result;
				if (_sensitivity != _currentSensitivity)
				{
					if (ctr == 0)
						ctr++;
					else
					{
						values.Add(_currentSensitivity, ctr);
						ctr = 1;
					}
					_currentSensitivity = _sensitivity;

				}
				else
				{
					ctr++;
				}
				count++;
				if (count == answers.Count)
					values.Add(_currentSensitivity, ctr);
			}

			return values;
		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/quiz/stats/lifestyle")]
		[HttpGet]
		public object GetLifestyle(string ecp_id = "", string age_group = "", string gender = "", string eyeglasses_frequency = "")
		{
			//var answers = db.GetLifeStyleDetail.AsEnumerable().Select(x => new LifeStyleDetail
			//{

			//	LifeStyleId = x.LifeStyleId,
			//	lifestyle = x.lifestyle


			//}).OrderBy(c => c.lifestyle).ToList<LifeStyleDetail>();
			if (!string.IsNullOrEmpty(age_group))
				if (age_group.Length > 1)
					if (age_group.Substring(0, 2) == "60")
						age_group = "60+";
			try
			{
				var answers = (from d in db.GetLifeStyleDetail.DefaultIfEmpty()
							   join f in db.GetQuizAnswer
								 on d.QuizAnswerId equals f.Id
							   where f.ECPId == (string.IsNullOrEmpty(ecp_id) ? f.ECPId : ecp_id)
																 && f.age_group == (string.IsNullOrEmpty(age_group) ? f.age_group : age_group)
																 && f.gender == (string.IsNullOrEmpty(gender) ? f.gender : gender)
																 && f.eyeglasses_frequency == (string.IsNullOrEmpty(eyeglasses_frequency) ? f.eyeglasses_frequency : eyeglasses_frequency)
							   select d
							 ).OrderBy(c => c.lifestyle).ToList();


				Dictionary<string, int> values = new Dictionary<string, int>();

				string _currentLifestyle = "";
				int ctr = 0;
				int count = 0;
				foreach (LifeStyleDetail answer in answers)
				{
					string _lifestyle = answer.lifestyle;
					if (_lifestyle != _currentLifestyle)
					{
						if (ctr == 0)
							ctr++;
						else
						{
							values.Add(_currentLifestyle, ctr);
							ctr = 1;
						}
						_currentLifestyle = _lifestyle;

					}
					else
					{
						ctr++;
					}
					count++;
					if (count == answers.Count)
						values.Add(_currentLifestyle, ctr);
				}

				return values;
			}
			catch (Exception)
			{
				Dictionary<string, int> values = new Dictionary<string, int>();
				return values;
			}

			
		}

	}
}