﻿using JWT.Algorithms;
using JWT.Builder;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using NewTransitionsAPI.Models;
using NewTransitionsAPI.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NewTransitionsAPI.Controllers
{

	public class EcpController : ApiController
	{
		private NewTransitionsContext db = new NewTransitionsContext();

		public IEnumerable<EcpViewModel> GetEcp()
		{

			var ecps = db.GetEcp.AsEnumerable().Select(x => new EcpViewModel
			{
				Id = x.Id,
				contactFullName = x.contactFullName,
				email = x.email,
				password = x.password,
				businessName = x.businessName,
				address = new Address(x.street, x.zipcode, x.state, x.city),
				phoneNumber = x.phoneNumber

			}).ToList<EcpViewModel>();

			return ecps;
		}


		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/ecp/signup2")]
		[HttpPost]
		[AcceptVerbs("OPTIONS")]
		public async Task<IHttpActionResult> PostSignup(EcpViewModel ecp)
		{
			try
			{
				Guid guid = Guid.NewGuid();
				const string secret = "GQDstcKsx0NHjPOuXOYg5MbeJ1XT0uFiwDVvVBrk";
				var token = new JwtBuilder()
				  .WithAlgorithm(new HMACSHA256Algorithm())
				  .WithSecret(secret)
				  .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds())
				  .AddClaim("claim2", "claim2-value")
				  .Build();

				var ecptoAdd = new Ecp
				{
					contactFullName = ecp.contactFullName,
					email = ecp.email,
					password = ecp.password,
					businessName = ecp.businessName,
					street = ecp.address.state,
					zipcode = ecp.address.zipcode,
					state = ecp.address.state,
					city = ecp.address.city,
					phoneNumber = ecp.phoneNumber,
					userID = guid,
					token = token
				};

				db.GetEcp.Add(ecptoAdd);
				db.SaveChanges();


				var response = new ECPResponse();
				response.id = guid;
				response.token = token;
				return Ok(response);

			}
			catch (Exception ex)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}


		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/ecp/login")]
		[HttpPost]
		[AcceptVerbs("OPTIONS")]
		public async Task<IHttpActionResult> GetOrder(EcpViewModel _input)
		{
			try
			{
				User _User = new User();
				var user = db.GetEcp.FirstOrDefault(
					p => p.email == _input.email && p.password == _input.password
					);


				_User.id = user.userID;
				_User.token = user.token;

				return Ok(_User);
			}
			catch (Exception ex)
			{
				return StatusCode(HttpStatusCode.BadRequest);
			}

		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/ecp/signup")]
		[HttpPost]
		[AcceptVerbs("OPTIONS")]
		public async Task<object> SignUser(object userObject)
		{
			var model = new oktaModel();
			var json = JsonConvert.SerializeObject(userObject);
			if (userObject != null)
			{
				try
				{

					using (var client = new HttpClient())
					{
						//this is where to put the token
						//var Token = ""; 
						System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
						string domain = ConfigurationManager.AppSettings["okta:OktaDomain"];
						string token = ConfigurationManager.AppSettings["okta:ClientId"];
						var webUrl = domain;
						var uri = "api/v1/users";
						client.BaseAddress = new Uri(webUrl);
						client.DefaultRequestHeaders.Add("Authorization", token);
						client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

						var result = await client.PostAsJsonAsync(uri, userObject);
						if ((int)result.StatusCode == 200)
						{
							var result_string = await result.Content.ReadAsStringAsync();
							var obj = JsonConvert.DeserializeObject<object>(result_string);
							return (obj);
						}
						else
						{
							var result_string = await result.Content.ReadAsStringAsync();
							//return StatusCode(HttpStatusCode.BadRequest);
							return BadRequest(result_string);
						}

					}



				}
				catch (Exception ex)
				{
					return BadRequest(ex.Message);
				}

			}
			else
			{

				return BadRequest("Object is null.");
			}

			//return model;

		}
		// GET: User
		[EnableCors("*", "*", "*")]
		[Route("api/ecp/{userid}")]
		[HttpGet()]
		[AcceptVerbs("OPTIONS")]
		public async Task<object> GetUser(string userid)
		{
			HttpResponseMessage testResult = null;
			HttpResponseMessage responseResult = null;
			HttpContext contextHeader = HttpContext.Current;

			var response_ = contextHeader.Response;

			response_.AddHeader("Access-Control-Allow-Origin", "*");

			if (contextHeader.Request.HttpMethod == "OPTIONS")
			{
				response_.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
				response_.AddHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
				response_.End();
			}

			//get the authorization header

			string authHeader = contextHeader.Request.Headers["Authorization"];
			string authHeaderTwo = null;
			string access_code = null;



			GetUserViewModel retuser = new GetUserViewModel();
			GetUserAddress address = new GetUserAddress();
			GetUserPhone phone = new GetUserPhone();

			if (!string.IsNullOrEmpty(authHeader))
			{
				authHeaderTwo = authHeader.Remove(authHeader.IndexOf("Bearer"), "Bearer".Length);
				access_code = authHeaderTwo.TrimStart();

			}

			//validate access_token
			var issuer = "https://dev-737335.oktapreview.com/oauth2/default";
			// var issuer = "https://dev-737335.oktapreview.com";

			var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(
				issuer + "/.well-known/oauth-authorization-server",
				new OpenIdConnectConfigurationRetriever(),
				new HttpDocumentRetriever());


			//const string auth0Domain = "https://dev-737335.oktapreview.com/oauth2/default/"; // Your Auth0 domain
			//const string auth0Audience = "api://default"; // Your API Identifier


			//IConfigurationManager<OpenIdConnectConfiguration> configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>($"{auth0Domain}.well-known/openid-configuration", new OpenIdConnectConfigurationRetriever());
			//OpenIdConnectConfiguration openIdConfig = await configurationManager.GetConfigurationAsync(CancellationToken.None);


			if (!string.IsNullOrEmpty(userid))
			{
				try
				{
					var validatedToken = await ValidateToken(access_code, issuer, configurationManager);

					var claimsList = validatedToken.Claims.ToList();
					var uid = claimsList[7].Value;


					if (validatedToken != null)
					{

						if (userid != uid)
						{
							using (var client = new HttpClient())
							{

								System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
								string domain = ConfigurationManager.AppSettings["okta:OktaDomain"];
								string token = ConfigurationManager.AppSettings["okta:ClientId"];
								var webUrl = domain;
								var uri = "/api/v1/users/";
								client.BaseAddress = new Uri(webUrl);


								using (var request = new HttpRequestMessage(new HttpMethod("GET"), webUrl + uri + userid))
								{




									request.Headers.TryAddWithoutValidation("Accept", "application/json");
									request.Headers.TryAddWithoutValidation("Authorization", token);



									var response = await client.SendAsync(request);

									if ((int)response.StatusCode == 200)
									{
										var result_string = await response.Content.ReadAsStringAsync();

										var obj = JsonConvert.DeserializeObject<object>(result_string);

										GetUserModel objtest = JsonConvert.DeserializeObject<GetUserModel>(result_string);



										//getusu

										retuser.id = objtest.id;
										retuser.contactFullName = objtest.profile.firstName + objtest.profile.lastName;
										retuser.email = objtest.profile.email;
										if (objtest.profile.credentials != null)
										{
											retuser.password = objtest.profile.credentials.password;
										}
										else
										{
											retuser.password = "blank password";
										}

										///retuser.password = objtest.profile.credentials.password: "blank password";
										retuser.businessName = objtest.profile.organization;
										address.zipcode = objtest.profile.zipCode;
										address.state = objtest.profile.state;
										address.city = objtest.profile.city;
										retuser.geUserAddress = address;
										phone.phoneNumber = objtest.profile.primaryPhone;
										retuser.getUserPhone = phone;
										return (retuser);
										// return (obj);
									}



								}

							}
						}
						else
						{
							return StatusCode(HttpStatusCode.BadRequest);
						}


					}
					else
					{
						return StatusCode(HttpStatusCode.BadRequest);
					}

				}




				//}
				catch (Exception ex)
				{

					throw ex.InnerException;
				}
			}
			//return testResult;

			return (retuser);
		}
		//update user
		[EnableCors("*", "*", "*")]
		[Route("api/ecp/{userId}")]
		[HttpPut()]
		[AcceptVerbs("OPTIONS")]
		public async Task<object> UpdateUser(string userId, object _oktaviewModel)
		{
			HttpResponseMessage testResult = null;
			HttpResponseMessage responseResult = null;
			HttpContext contextHeader = HttpContext.Current;


			//get the authorization header

			string authHeader = contextHeader.Request.Headers["Authorization"];
			string authHeaderTwo = null;
			string access_code = null;




			var issuer = "https://dev-737335.oktapreview.com/oauth2/default";

			var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(
				issuer + "/.well-known/oauth-authorization-server",
				new OpenIdConnectConfigurationRetriever(),
				new HttpDocumentRetriever());


			UpdateUserViewModel retVal = new UpdateUserViewModel();

			if (!string.IsNullOrEmpty(authHeader))
			{
				authHeaderTwo = authHeader.Remove(authHeader.IndexOf("Bearer"), "Bearer".Length);
				access_code = authHeaderTwo.TrimStart();

			}


			try
			{
				var validatedToken = await ValidateToken(access_code, issuer, configurationManager);

				if (validatedToken != null)
				{

					var claimsList = validatedToken.Claims.ToList();
					var uid = claimsList[7].Value;


					if (uid != userId)
					{

						if (!string.IsNullOrEmpty(userId) && _oktaviewModel != null)
						{
							using (var client = new HttpClient())
							{


								System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
								string domain = ConfigurationManager.AppSettings["okta:OktaDomain"];
								string token = ConfigurationManager.AppSettings["okta:ClientId"];
								var webUrl = domain;
								var uri = "/api/v1/users/" + userId;
								client.BaseAddress = new Uri(webUrl);
								client.DefaultRequestHeaders.Add("Authorization", token);
								client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

								var result = await client.PutAsJsonAsync(uri, _oktaviewModel);

								if ((int)result.StatusCode == 200)
								{
									var result_string = await result.Content.ReadAsStringAsync();
									var obj = JsonConvert.DeserializeObject<object>(result_string);

									retVal.id = userId;
									retVal.token = access_code;
									return (retVal);
								}
								else
								{
									//var result_string = await result.Content.ReadAsStringAsync();
									////return StatusCode(HttpStatusCode.BadRequest);
									//return BadRequest(result_string);
									return StatusCode(HttpStatusCode.BadRequest);
								}

							}
						}
					}
				}
				else
				{
					return StatusCode(HttpStatusCode.BadRequest);
				}
			}
			catch (Exception ex)
			{

				throw ex.InnerException;
			}
			return (retVal);
		}
		[EnableCors("*", "*", "*")]
		[Route("api/ecp/{userid}/change_password")]
		[HttpPost()]
		public async Task<object> UpdatePassword(string userid, object credentials)
		{
			HttpResponseMessage testResult = null;
			HttpResponseMessage responseResult = null;
			HttpContext contextHeader = HttpContext.Current;


			//get the authorization header

			string authHeader = contextHeader.Request.Headers["Authorization"];
			string authHeaderTwo = null;
			string access_code = null;




			var issuer = "https://dev-737335.oktapreview.com/oauth2/default";
			//var issuer = "https://dev-737335.oktapreview.com";

			var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(
				issuer + "/.well-known/oauth-authorization-server",
				new OpenIdConnectConfigurationRetriever(),
				new HttpDocumentRetriever());

			if (!string.IsNullOrEmpty(authHeader))
			{
				authHeaderTwo = authHeader.Remove(authHeader.IndexOf("Bearer"), "Bearer".Length);
				access_code = authHeaderTwo.TrimStart();

			}

			UpdateUserViewModel retVal = new UpdateUserViewModel();

			try
			{

				var validatedToken = await ValidateToken(access_code, issuer, configurationManager);


				if (!string.IsNullOrEmpty(userid) && credentials != null)
				{

					if (validatedToken != null)
					{

						var claimsList = validatedToken.Claims.ToList();
						var uid = claimsList[7].Value;


						if (uid == userid)
						{
							using (var client = new HttpClient())
							{


								System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
								string domain = ConfigurationManager.AppSettings["okta:OktaDomain"];
								string token = ConfigurationManager.AppSettings["okta:ClientId"];
								var webUrl = domain;
								var uri = "api/v1/users/" + userid + "/credentials/change_password";
								client.BaseAddress = new Uri(webUrl);
								client.DefaultRequestHeaders.Add("Authorization", token);
								client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

								var result = await client.PostAsJsonAsync(uri, credentials);

								if ((int)result.StatusCode == 200)
								{

									retVal.id = userid;
									retVal.token = access_code;
									return (retVal);
								}



							}
						}
						else
						{
							return StatusCode(HttpStatusCode.BadRequest);
						}



					}
					else
					{
						return StatusCode(HttpStatusCode.BadRequest);

					}




				}

			}
			catch (Exception ex)
			{

				throw ex.InnerException;
			}
			return retVal;
		}
		private async Task<HttpResponseMessage> OktaMethod(string access_token)
		{
			HttpResponseMessage oktaResult = null;
			try
			{
				using (var client = new HttpClient())
				{
					System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
					string domain = ConfigurationManager.AppSettings["okta:OktaDomain"];
					string token = ConfigurationManager.AppSettings["okta:ClientId"];
					var webUrl = domain;
					//var uri = domain + "/oauth2/v1/introspect";
					var uri = "https://dev-737335.oktapreview.com/oauth2/SSWS00TsFWGFOwcZ3FWIbzt0gYAa5ZWVS0Evu__ylx9Hun/v1/introspect";
					client.BaseAddress = new Uri(webUrl);
					client.DefaultRequestHeaders.Add("Authorization", token);
					client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

					var result = await client.PostAsJsonAsync(uri, access_token);
					oktaResult = result;
				}
			}
			catch (Exception)
			{

				throw;
			}

			return oktaResult;

		}
		private static async Task<JwtSecurityToken> ValidateToken(string token, string issuer, IConfigurationManager<OpenIdConnectConfiguration> configurationManager,
			CancellationToken ct = default(CancellationToken))
		{
			if (string.IsNullOrEmpty(token)) throw new ArgumentNullException(nameof(token));
			if (string.IsNullOrEmpty(issuer)) throw new ArgumentNullException(nameof(issuer));


			var discoveryDocument = await configurationManager.GetConfigurationAsync(ct);
			//var uid = discoveryDocument.ClaimsSupported;
			//string testuid = null;
			//foreach (var item in uid)
			//{
			//   testuid =   item[7].ToString();
			//}


			var signingKeys = discoveryDocument.SigningKeys;

			var validationParameters = new TokenValidationParameters
			{

				ValidIssuer = issuer,
				ValidAudiences = new[] { "api://default" },
				IssuerSigningKeys = signingKeys


			};

			try
			{
				SecurityToken rawValidatedToken;
				var principal = new JwtSecurityTokenHandler()
					.ValidateToken(token, validationParameters, out rawValidatedToken);
				return (JwtSecurityToken)rawValidatedToken;
			}
			catch (SecurityTokenValidationException ex)
			{

				return null;
			}
		}
		private static bool VerifySignature(string accesstoken)
		{

			bool retval = false;
			RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
			rsa.ImportParameters(
			  new RSAParameters()
			  {
				  Modulus = FromBase64Url("w7Zdfmece8iaB0kiTY8pCtiBtzbptJmP28nSWwtdjRu0f2GFpajvWE4VhfJAjEsOcwYzay7XGN0b-X84BfC8hmCTOj2b2eHT7NsZegFPKRUQzJ9wW8ipn_aDJWMGDuB1XyqT1E7DYqjUCEOD1b4FLpy_xPn6oV_TYOfQ9fZdbE5HGxJUzekuGcOKqOQ8M7wfYHhHHLxGpQVgL0apWuP2gDDOdTtpuld4D2LK1MZK99s9gaSjRHE8JDb1Z4IGhEcEyzkxswVdPndUWzfvWBBWXWxtSUvQGBRkuy1BHOa4sP6FKjWEeeF7gm7UMs2Nm2QUgNZw6xvEDGaLk4KASdIxRQ"),
				  Exponent = FromBase64Url("AQAB")
			  });

			var validationParameters = new TokenValidationParameters
			{
				RequireExpirationTime = false,
				RequireSignedTokens = false,
				ValidateAudience = false,
				ValidateIssuer = false,
				ValidateLifetime = false,
				IssuerSigningKey = new RsaSecurityKey(rsa)
			};

			SecurityToken validatedSecurityToken = null;
			var handler = new JwtSecurityTokenHandler();
			handler.ValidateToken(accesstoken, validationParameters, out validatedSecurityToken);
			JwtSecurityToken validatedJwt = validatedSecurityToken as JwtSecurityToken;

			return (retval);

		}
		static byte[] FromBase64Url(string base64Url)
		{
			string padded = base64Url.Length % 4 == 0
				? base64Url : base64Url + "====".Substring(base64Url.Length % 4);
			string base64 = padded.Replace("_", "/")
								  .Replace("-", "+");
			return Convert.FromBase64String(base64);

		}
		private static bool Validate(string authToken)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			var validationParameters = GetValidationParameters();

			SecurityToken validatedToken;
			IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
			return true;
		}
		private static TokenValidationParameters GetValidationParameters()
		{
			return new TokenValidationParameters()
			{
				ValidateLifetime = false, // Because there is no expiration in the generated token
				ValidateAudience = false, // Because there is no audiance in the generated token
				ValidateIssuer = false,   // Because there is no issuer in the generated token
				ValidIssuer = "https://dev-737335.oktapreview.com",
				ValidAudience = "https://dev-737335.oktapreview.com"
				//IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)) // The same key as the one that generate the token
			};
		}
		private async Task<HttpResponseMessage> GetUserInfo(string accesstoken)
		{
			HttpResponseMessage oktaResult = null;
			try
			{
				using (var client = new HttpClient())
				{
					System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
					string domain = ConfigurationManager.AppSettings["okta:OktaDomain"];
					string token = ConfigurationManager.AppSettings["okta:ClientId"];
					var webUrl = domain;
					var uri = "v1/userinfo";
					client.BaseAddress = new Uri(webUrl);
					client.BaseAddress = new Uri(webUrl);
					client.DefaultRequestHeaders.Add("Authorization", "Bearer" + accesstoken);
					client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
					var result = await client.GetAsync(uri);

				}
			}

			catch (Exception)
			{

				throw;
			}

			return oktaResult;
		}
	}
}