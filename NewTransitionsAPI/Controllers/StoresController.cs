﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.UI.HtmlControls;
using NewTransitionsAPI.Models;
using NewTransitionsAPI.Models.ViewModel;
using System.Xml;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Protocols;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace NewTransitionsAPI.Controllers
{
	public class StoresController : ApiController
	{


		public enum EcpType { All, Independent, Retail };
		public enum SearchMode { DirectPost, QueryString };
		public enum Country
		{
			US = 233,
			CA = 40
		}

		private SearchMode mode = SearchMode.DirectPost;
		public SearchMode Mode
		{
			get { return mode; }
			set { mode = value; }
		}

		private EcpType ecpTypeFilter = EcpType.All;
		public EcpType EcpTypeFilter
		{
			get { return ecpTypeFilter; }
			set { ecpTypeFilter = value; }
		}

		private string brandFilter = "";
		public string BrandFilter
		{
			get { return brandFilter; }
			set { brandFilter = value; }
		}


		// GET: stores




		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/stores")]
		[HttpGet()]
		public async Task<List<StoreViewModel>> GetStores(string lat, string lng, int? perimeter)
		{
			try
			{
				GoogleAddressParam _param = await GetPostalCode(lat, lng);

				Guid guid = Guid.NewGuid();

				string zip = _param.zipCode;
				string city = "";
				int stateID = 0;
				int countryID = (int)((Country)Enum.Parse(typeof(Country), _param.country)); ;
				//int distance = perimeter;
				string unit = "M";
				Int64 searchID;

				//int totalOverallCount = 0;

				string inpSearchResults = "";
				string accessCode = ConfigurationManager.AppSettings["webServiceAccessCode"];
				int maxReturn = 100;



				List<StoreViewModel> response = new List<StoreViewModel>();

				List<com.transitions.practicelocator.Ecp> filtered = new List<com.transitions.practicelocator.Ecp>();


				if (!string.IsNullOrEmpty(zip) && perimeter.HasValue && countryID > 0)
				{
					var distance = perimeter;
					com.transitions.practicelocator.TOIECPLocatorService srv = new com.transitions.practicelocator.TOIECPLocatorService();

					var data = srv.GetSearchID(accessCode, zip, city, stateID, countryID, Convert.ToInt32(perimeter), Convert.ToChar(unit));

					if (data > 0)
					{
						var ecplist = srv.GetEcpSearchList(accessCode, data, ecpTypeFilter.ToString().Substring(0, 1), brandFilter);

						if (ecplist.Length > 0)
						{
							for (int i = 0; i < ecplist.Length; i++)
							{
								if (ecplist[i].Distance < distance)
									filtered.Add(ecplist[i]);

							}

							foreach (var item in filtered)
							{

								StoreViewModel bodyResponse = new StoreViewModel();
								//PrimaryData primaryData = new PrimaryData();
								Geolocation geo = new Geolocation();
								StoreAddress storeAddres = new StoreAddress();
								Distance _distance = new Distance();
								//Contacts contacts = new Contacts();

								bodyResponse.id = item.ToiID;
								bodyResponse.name = item.EcpName;
								geo.lat = item.Latitiude;
								geo.longi = item.Longitude;
								storeAddres.streetAddress = item.Address1;
								storeAddres.zipCode = item.Zip;
								storeAddres.city = item.City;
								storeAddres.state = item.State;
								_distance.value = item.Distance;
								_distance.unit = item.DistanceUnit;
								bodyResponse.primaryPhone = item.Phone;
								bodyResponse.email = item.Email;
								bodyResponse.geolocation = geo;
								bodyResponse.storeAddress = storeAddres;
								bodyResponse.website = item.Website;
								bodyResponse.distance = _distance;
								response.Add(bodyResponse);

								if (response.Count >= maxReturn)
									break;


							}


						}


					}


				}
				else
				{
					throw new HttpResponseException(HttpStatusCode.BadRequest);



				}

				return response;
			}
			catch (Exception)
			{
				throw new HttpResponseException(HttpStatusCode.BadRequest);
			}


		}

		[EnableCors(origins: "*", headers: "*", methods: "*")]
		[Route("api/stores/{storeId}")]
		[HttpGet()]
		public async Task<StoreViewModel> GetStore(string storeId, string lat = "", string lng = "")
		{
			try
			{
				string accessCode = ConfigurationManager.AppSettings["webServiceAccessCode"];

				StoreViewModel bodyResponse = new StoreViewModel();
				Geolocation geo = new Geolocation();
				StoreAddress storeAddres = new StoreAddress();
				Distance _distance = new Distance();

				//com.transitions.practicelocator.TOIECPLocatorService srv = new com.transitions.practicelocator.TOIECPLocatorService();
				//srv.GetEcpAsync(accessCode, storeId);
				//com.transitions.practicelocator.Ecp item = srv.GetEcp(accessCode, storeId);
				string strcon = System.Configuration.ConfigurationManager.ConnectionStrings["DBTOIECPConnection"].ConnectionString;
				using (var connection = new SqlConnection(strcon))
				{
					using (var command = new SqlCommand("GetEcpNew", connection))
					{
						command.CommandType = CommandType.StoredProcedure;
						command.Parameters.AddWithValue("@toiID", storeId);
						command.Parameters.AddWithValue("@lat", (!string.IsNullOrEmpty(lat) ? Convert.ToDouble(lat) : Convert.ToDouble("0")));
						command.Parameters.AddWithValue("@lon", (!string.IsNullOrEmpty(lng) ? Convert.ToDouble(lng) : Convert.ToDouble("0")));
						connection.Open();
						var reader = command.ExecuteReader();

						while (reader.Read())
						{
							bodyResponse.id = (reader["toiID"] != null ? reader["toiID"].ToString() : "");
							bodyResponse.name = (reader["ecpName"] != null ? reader["ecpName"].ToString() : "");
							geo.lat = (reader["latitude"] != null ? Convert.ToDouble(reader["latitude"].ToString()) : Convert.ToDouble("0"));
							geo.longi = (reader["longitude"] != null ? Convert.ToDouble(reader["longitude"].ToString()) : Convert.ToDouble("0")); ;
							storeAddres.streetAddress = (reader["address1"] != null ? reader["address1"].ToString() : "") + (reader["address2"] != null ? " " + reader["address2"].ToString() : "") + (reader["address3"] != null ? " " + reader["address3"].ToString() : "");
							storeAddres.streetAddress = storeAddres.streetAddress != null ? storeAddres.streetAddress.Trim() : storeAddres.streetAddress;
							storeAddres.zipCode = (reader["zip"] != null ? reader["zip"].ToString() : "");
							storeAddres.city = (reader["city"] != null ? reader["city"].ToString() : "");
							storeAddres.state = (reader["state"] != null ? reader["state"].ToString() : "");
							if (!string.IsNullOrEmpty(lat) && !string.IsNullOrEmpty(lng))
							{
								_distance.value = (reader["distance"] != null ? Convert.ToDouble(reader["distance"].ToString()) : Convert.ToDouble("0"));
								_distance.unit = (reader["distanceUnit"] != null ? reader["distanceUnit"].ToString() : "");
								bodyResponse.distance = _distance;
							}
							bodyResponse.primaryPhone = (reader["phone"] != null ? reader["phone"].ToString() : "");
							bodyResponse.email = (reader["email"] != null ? reader["email"].ToString() : "");
							bodyResponse.geolocation = geo;
							bodyResponse.storeAddress = storeAddres;
							bodyResponse.website = (reader["website"] != null ? reader["website"].ToString() : "");
						}
					}
					connection.Close();
					connection.Dispose();
				} 

				return bodyResponse;
			}
			catch (Exception ex)
			{
				throw new HttpResponseException(HttpStatusCode.BadRequest);
			}


		}

		async Task<GoogleAddressParam> GetPostalCode(string lat, string lng)
		{
			GoogleAddressParam _return = new GoogleAddressParam();

			using (var client = new HttpClient())
			{

				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
				var webUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + ConfigurationManager.AppSettings["googleMapApiKey"]; ;

				client.BaseAddress = new Uri(webUrl);


				using (var request = new HttpRequestMessage(new HttpMethod("GET"), webUrl))
				{

					request.Headers.TryAddWithoutValidation("Accept", "application/json");

					var response = await client.SendAsync(request);

					if ((int)response.StatusCode == 200)
					{
						var result_string = await response.Content.ReadAsStringAsync();

						var obj = JsonConvert.DeserializeObject<object>(result_string);

						GoogleAddressModel objtest = JsonConvert.DeserializeObject<GoogleAddressModel>(result_string);

						foreach (results _results in objtest.results)
						{
							foreach (address_components _address_components in _results.address_components)
							{
								foreach (string item in _address_components.types)
								{
									if (item == "postal_code")
									{
										_return.zipCode = _address_components.long_name;
									}
									if (item == "country")
									{
										_return.country = _address_components.short_name;
									}
									if (!string.IsNullOrEmpty(_return.zipCode) && !string.IsNullOrEmpty(_return.country))
										return _return;
								}
							}
						}

					}



				}

			}
			//if (_return.zipCode == null)
			//{
			//	_return = new GoogleAddressParam();
			//	_return.country = "US";
			//	_return.zipCode = "75001";
			//}
			return _return;
		}
	}
}