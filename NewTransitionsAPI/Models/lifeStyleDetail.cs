﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class LifeStyleDetail
	{
		[Key]
		public int LifeStyleId { get; set; }

		public string lifestyle { get; set; }

		[ForeignKey("QuizAnswer")]
		public int QuizAnswerId { get; set; }
		public QuizAnswer QuizAnswer { get; set; }
	}
}