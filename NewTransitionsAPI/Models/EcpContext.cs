﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class EcpContext: DbContext
	{
		public EcpContext() : base("name=USECPContext")
		{
		}

		public DbSet<Ecp> GetEcp { get; set; }
	}
}