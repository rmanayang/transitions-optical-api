﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class ECPResponse
	{
		[JsonProperty("id")]
		public Guid id { get; set; }


		[JsonProperty("token")]
		public string token { get; set; }

	}
}