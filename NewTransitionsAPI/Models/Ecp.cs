﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class Ecp
	{
		[Key]
		public int Id { get; set; }

		public string contactFullName { get; set; }

		[Required]
		public string email { get; set; }

		[Required]
		public string password { get; set; }
		public string businessName { get; set; }

		[Required]
		public string street { get; set; }
		[Required]
		public string zipcode { get; set; }
		[Required]
		public string state { get; set; }
		[Required]
		public string city { get; set; }

		public string phoneNumber { get; set; }
		public Guid userID { get; set; }
		public string token { get; set; } 
	}
}