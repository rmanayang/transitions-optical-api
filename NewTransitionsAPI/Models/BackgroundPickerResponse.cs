﻿using Newtonsoft.Json;
using NewTransitionsAPI.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class BackgroundPickerResponse
	{


		[JsonProperty("data")]
		public AnswerData AnswerData { get; set; }


		[JsonProperty("status")]
		public string Status { get; set; }  

		[JsonProperty("errorMessage")]
		public string ErrorMessage { get; set; }
	}

	public class AnswerData
	{

		[JsonProperty("result_id")]
		public Guid ResultId { get; set; }

		[JsonProperty("sensitivity")]
		public string Sensitivity { get; set; }

		[JsonProperty("answers")]
		public AnswerViewModel Answers { get; set; }

		[JsonProperty("demographics")]
		public DemographicsModel Demographics { get; set; }
	}
}