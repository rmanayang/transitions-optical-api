﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models.ViewModel
{
    public class StoreViewModel
    {

        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("geolocation")]
        public Geolocation geolocation { get; set; }
        [JsonProperty("Address")]
        public StoreAddress storeAddress { get; set; }
        //[JsonProperty("contacts")]
        //public Contacts contacts { get; set; }
        [JsonProperty("primaryPhone")]
        public string primaryPhone { get; set; }
        [JsonProperty("email")]
        public string email { get; set; }
		[JsonProperty("distance")]
		public Distance distance { get; set; }
		[JsonProperty("website")]
		public string website { get; set; }

	}
    public class PrimaryData
    {
        [JsonProperty("id")]
        public string id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
    }
    public class Geolocation
    {
        [JsonProperty("lat")]
        public double lat { get; set; }
        [JsonProperty("longi")]
        public double longi { get; set; }

    }

    public class StoreAddress
    {
        [JsonProperty("streetAddress")]
        public string streetAddress { get; set; }
        [JsonProperty("zipCode")]
        public string zipCode { get; set; }
        [JsonProperty("city")]
        public string city { get; set; }
        [JsonProperty("state")]
        public string state { get; set; }
    }
    public class Contacts
    {
        [JsonProperty("primaryPhone")]
        public string primaryPhone { get; set; }
        [JsonProperty("email")]
        public string email { get; set; }

	}
	public class Distance
	{
		[JsonProperty("value")]
		public double value { get; set; }
		[JsonProperty("unit")]
		public string unit { get; set; }

	}
}