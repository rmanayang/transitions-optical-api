﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class BackgroundPickerContext : DbContext
	{
		public BackgroundPickerContext() : base("name=BackgroundPickerContext")
		{
		}

		public DbSet<BackgroundPicker> GetFullNames { get; set; }
	}
}