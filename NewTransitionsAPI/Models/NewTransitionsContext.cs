﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class NewTransitionsContext : DbContext
	{
		public NewTransitionsContext() : base("name=NewTransitionsAPIContext")
		{
		}

		public DbSet<QuizAnswer> GetQuizAnswer { get; set; }

		public DbSet<Ecp> GetEcp { get; set; }

		public DbSet<LifeStyleDetail> GetLifeStyleDetail { get; set; }
	}
}