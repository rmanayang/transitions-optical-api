﻿using NewTransitionsAPI.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class QuizAnswer
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public bool Answer1 { get; set; }

		[Required]
		public bool Answer2 { get; set; }

		[Required]
		public bool Answer3 { get; set; }

		[Required]
		public bool Answer4 { get; set; }

		[Required]
		public bool Answer5 { get; set; }

		[Required]
		public bool Answer6 { get; set; }

		public string ECPId { get; set; }
		public Guid ResultId { get; set; }
		public string Result { get; set; }
		public DateTime? DateCreated { get; set; }

		public string eyeglasses_frequency { get; set; }
		public string gender { get; set; }
		public string age_group { get; set; }

		public List<string> LifeStyleDetail { get; set; }
	}
	 
}