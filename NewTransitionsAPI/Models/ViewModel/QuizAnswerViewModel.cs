﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models.ViewModel
{
	public class QuizAnswerViewModel
	{
		[JsonProperty("Id")]
		public int Id { get; set; }
		[JsonProperty("answers")]
		public AnswerViewModel Answers { get; set; }
		[JsonProperty("ecp_id")]
		public string ECPId { get; set; }
		[JsonProperty("resultid")]
		public Guid ResultId { get; set; }
		[JsonProperty("result")]
		public string Result { get; set; }
		[JsonProperty("DateCreated")]
		public DateTime? DateCreated { get; set; }

		[JsonProperty("demographics")]
		public DemographicsModel Demographics { get; set; }

	}
	
	public class DemographicsModel
	{
		private NewTransitionsContext db = new NewTransitionsContext();
		//public DemographicsModel(string _eyeglasses_frequency, string _gender, string _age_group, List<LifeStyle> _LifeStyle)
		//{
		//	eyeglasses_frequency = _eyeglasses_frequency;
		//	gender = _gender;
		//	age_group = _age_group;
		//	LifeStyle = _LifeStyle;
		//}
		public DemographicsModel(string _eyeglasses_frequency, string _gender, string _age_group, int id)
		{
			eyeglasses_frequency = _eyeglasses_frequency;
			gender = _gender;
			age_group = _age_group;

			try
			{
				var _lifestyle = db.GetLifeStyleDetail.AsEnumerable().Where(x => x.QuizAnswerId == id).Select(x => new LifeStyleDetail
				{

					LifeStyleId = x.LifeStyleId,
					lifestyle = x.lifestyle

				}).ToList<LifeStyleDetail>();

				List<string> _temp = new List<string>();
				foreach (LifeStyleDetail item in _lifestyle)
				{
					_temp.Add(item.lifestyle);
				}
				LifeStyle = _temp;
			}
			catch (Exception ex)
			{
				List<string> _temp = new List<string>();
				LifeStyle = _temp;
			}
			
		}
		[JsonProperty("eyeglasses_frequency")]
		public string eyeglasses_frequency { get; set; }
		[JsonProperty("gender")]
		public string gender { get; set; }
		[JsonProperty("age_group")]
		public string age_group { get; set; }

		[JsonProperty("lifestyle")]
		public List<string> LifeStyle { get; set; }
	}
	public class AnswerViewModel
	{
		public AnswerViewModel()
		{

		}
		public AnswerViewModel(bool _answer1, bool _answer2, bool _answer3, bool _answer4, bool _answer5, bool _answer6)
		{
			Answer1 = _answer1;
			Answer2 = _answer2;
			Answer3 = _answer3;
			Answer4 = _answer4;
			Answer5 = _answer5;
			Answer6 = _answer6;
		}

		[JsonProperty("answer1")]
		public bool Answer1 { get; set; }
		[JsonProperty("answer2")]
		public bool Answer2 { get; set; }
		[JsonProperty("answer3")]
		public bool Answer3 { get; set; }
		[JsonProperty("answer4")]
		public bool Answer4 { get; set; }
		[JsonProperty("answer5")]
		public bool Answer5 { get; set; }
		[JsonProperty("answer6")]
		public bool Answer6 { get; set; }
	}

	public class LifeStyle
	{
		[JsonProperty("lifestyle")]
		public string lifestyle { get; set; }
	}

	public class ActivityViewModel
	{
		[JsonProperty("data")]
		public List<Dictionary<string,int>> _list { get; set; }
	}

	public class Activity
	{
		[JsonProperty("date1")]
		public int date1 { get; set; }
	}
}