﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models.ViewModel
{
	public class GetUserModel
	{
		[JsonProperty("id")]
		public string id { get; set; }
		[JsonProperty("status")]
		public string status { get; set; }
		[JsonProperty("created")]
		public string created { get; set; }
		[JsonProperty("statusChanged")]
		public string statusChanged { get; set; }
		[JsonProperty("lastLogin")]
		public string lastLogin { get; set; }
		[JsonProperty("address")]
		public GetUserAddress address { get; set; }
		[JsonProperty("passwordChanged")]
		public string passwordChanged { get; set; }
		[JsonProperty("profile")]
		public GetUserModelProfile profile { get; set; }


	}
	public class GetUserModelProfile
	{
		[JsonProperty("profileUrl")]
		public string profileUrl { get; set; }
		[JsonProperty("lastName")]
		public string lastName { get; set; }
		[JsonProperty("zipCode")]
		public string zipCode { get; set; }
		[JsonProperty("preferredLanguage")]
		public string preferredLanguage { get; set; }
		[JsonProperty("city")]
		public string city { get; set; }
		[JsonProperty("displayName")]
		public string displayName { get; set; }
		[JsonProperty("costCenter")]
		public string costCenter { get; set; }
		[JsonProperty("secondEmail")]
		public string secondEmail { get; set; }
		[JsonProperty("title")]
		public string title { get; set; }
		[JsonProperty("login")]
		public string login { get; set; }

		[JsonProperty("employeeNumber")]
		public string employeeNumber { get; set; }

		[JsonProperty("firstName")]
		public string firstName { get; set; }

		[JsonProperty("primaryPhone")]
		public string primaryPhone { get; set; }
		[JsonProperty("mobilePhone")]
		public string mobilePhone { get; set; }

		[JsonProperty("streetAddress")]
		public string streetAddress { get; set; }

		[JsonProperty("countrycode")]
		public string countrycode { get; set; }

		[JsonProperty("organization")]
		public string organization { get; set; }

		[JsonProperty("userType")]
		public string userType { get; set; }

		[JsonProperty("state")]
		public string state { get; set; }

		[JsonProperty("department")]
		public string department { get; set; }

		[JsonProperty("email")]
		public string email { get; set; }
		[JsonProperty("credentials")]
		public GetUserModelCredentials credentials { get; set; }

	}
	public class GetUserModelCredentials
	{
		[JsonProperty("password")]
		public string password { get; set; }
		[JsonProperty("email")]
		public string email { get; set; }
	}
}