﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models.ViewModel
{
	public class GoogleAddressModel
	{
		[JsonProperty("plus_code")]
		public plus_code plus_code { get; set; }

		[JsonProperty("results")]
		public List<results> results { get; set; }
	}

	public class results
	{
		[JsonProperty("address_components")]
		public List<address_components> address_components { get; set; }
	}

	public class plus_code
	{
		[JsonProperty("compound_code")]
		public string compound_code { get; set; }

		[JsonProperty("global_code")]
		public string global_code { get; set; }
	}

	public class address_components
	{
		[JsonProperty("long_name")]
		public string long_name { get; set; }

		[JsonProperty("short_name")]
		public string short_name { get; set; }


		[JsonProperty("types")]
		public string[] types { get; set; }
	}

	public class GoogleAddressParam
	{
		public string zipCode { get; set; }
		public string country { get; set; }
	}
}