﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models.ViewModel
{
	public class BackgroundPickerViewModel
	{
		[JsonProperty("Id")]
		public int Id { get; set; }
		[JsonProperty("FullName")]
		public string FullName { get; set; }
		[JsonProperty("Background")]
		public string Background { get; set; }
		[JsonProperty("DateCreated")]
		public DateTime? DateCreated { get; set; }
	}
}