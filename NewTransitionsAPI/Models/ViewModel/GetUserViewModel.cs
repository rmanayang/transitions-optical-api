﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models.ViewModel
{
	public class GetUserViewModel
	{
		[JsonProperty("id")]
		public string id { get; set; }
		[JsonProperty("contactFullName")]
		public string contactFullName { get; set; }
		[JsonProperty("email")]
		public string email { get; set; }
		[JsonProperty("password")]
		public string password { get; set; }
		[JsonProperty("businessName")]
		public string businessName { get; set; }
		[JsonProperty("address")]
		public GetUserAddress geUserAddress { get; set; }
		[JsonProperty("phoneNumber")]
		public GetUserPhone getUserPhone { get; set; }
	}

	public class GetUserAddress
	{
		[JsonProperty("street")]
		public string street { get; set; }
		[JsonProperty("zipcode")]
		public string zipcode { get; set; }
		[JsonProperty("state")]
		public string state { get; set; }
		[JsonProperty("city")]
		public string city { get; set; }
	}
	public class GetUserPhone
	{
		[JsonProperty("phoneNumber")]
		public string phoneNumber { get; set; }
	}
}