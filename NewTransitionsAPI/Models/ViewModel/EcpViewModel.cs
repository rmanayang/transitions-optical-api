﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models.ViewModel
{
	public class EcpViewModel
	{
		[JsonProperty("Id")]
		public int Id { get; set; }
		[JsonProperty("contactFullName")]
		public string contactFullName { get; set; }
		[JsonProperty("email")]
		public string email { get; set; }
		[JsonProperty("password")]
		public string password { get; set; }
		[JsonProperty("businessName")]
		public string businessName { get; set; }
		[JsonProperty("address")]
		public Address address { get; set; }
		[JsonProperty("phoneNumber")]
		public string phoneNumber { get; set; }
	}

	public class Address
	{
		public Address(string _street, string _zipcode, string _state, string _city)
		{
			street = _street;
			zipcode = _zipcode;
			state = _state;
			city = _city;
		}

		[JsonProperty("street")]
		public string street { get; set; }
		[JsonProperty("zipcode")]
		public string zipcode { get; set; }
		[JsonProperty("state")]
		public string state { get; set; }
		[JsonProperty("city")]
		public string city { get; set; }
	}

	public class User
	{
		[JsonProperty("id")]
		public Guid id { get; set; }
		[JsonProperty("token")]
		public string token { get; set; }
	}

	public class oktaModel
	{
		public string Id { get; set; }
		public string token { get; set; }
	}

	public class ECPOktaModel
	{
		[JsonProperty("profile")]
		public Profile profile { get; set; }

		[JsonProperty("credentials")]
		public Credentials credentials { get; set; }

		[JsonProperty("groupIds")]
		public List<string> groupIds { get; set; }

	}

	public class Profile
	{
		[JsonProperty("firstName")]
		public string firstName { get; set; }
		[JsonProperty("lastName")]
		public string lastName { get; set; }
		[JsonProperty("email")]
		public string email { get; set; }
		[JsonProperty("login")]
		public string login { get; set; }
		[JsonProperty("organization")]
		public Address organization { get; set; }
		[JsonProperty("streetAddress")]
		public string streetAddress { get; set; }
		[JsonProperty("zipCode")]
		public string zipCode { get; set; }
		[JsonProperty("city")]
		public string city { get; set; }
		[JsonProperty("state")]
		public string state { get; set; }
		[JsonProperty("primaryPhone")]
		public string primaryPhone { get; set; }
	}

	public class Credentials
	{
		[JsonProperty("password")]
		public Password password { get; set; }
	}
	public class Password
	{
		[JsonProperty("value")]
		public string value { get; set; }
	}
	public class GroupIds
	{
		[JsonProperty("password")]
		public List<string> password { get; set; }
	}
}