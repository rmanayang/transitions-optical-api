﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NewTransitionsAPI.Models
{
	public class BackgroundPicker
	{
		[Key]
		public int Id { get; set; }

		public string FullName { get; set; }
		public string Background { get; set; }
		public DateTime? DateCreated { get; set; }

	}
}