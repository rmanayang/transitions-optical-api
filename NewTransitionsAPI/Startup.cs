﻿using Microsoft.Owin;
using Owin;
using System.Net;
using System.Web;
using System.Web.Http;

[assembly: OwinStartupAttribute(typeof(NewTransitionsAPI.Startup))]
namespace NewTransitionsAPI
{
	public partial class Startup
	{


		public void Configuration(IAppBuilder app)
		{
			var context = HttpContext.Current;
			var response = context.Response;

			response.AddHeader("Access-Control-Allow-Origin", "*");

			if (context.Request.HttpMethod == "OPTIONS")
			{
				response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
				response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
				response.End();
			}

			//HttpConfiguration config = new HttpConfiguration();
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

			ConfigureAuth(app);

			//WebApiConfig.Register(config);
			//app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);


		}
	}
}