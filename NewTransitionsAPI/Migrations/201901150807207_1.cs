namespace NewTransitionsAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Ecps", "email", c => c.String(nullable: false));
            AlterColumn("dbo.Ecps", "password", c => c.String(nullable: false));
            AlterColumn("dbo.Ecps", "street", c => c.String(nullable: false));
            AlterColumn("dbo.Ecps", "zipcode", c => c.String(nullable: false));
            AlterColumn("dbo.Ecps", "state", c => c.String(nullable: false));
            AlterColumn("dbo.Ecps", "city", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Ecps", "city", c => c.String());
            AlterColumn("dbo.Ecps", "state", c => c.String());
            AlterColumn("dbo.Ecps", "zipcode", c => c.String());
            AlterColumn("dbo.Ecps", "street", c => c.String());
            AlterColumn("dbo.Ecps", "password", c => c.String());
            AlterColumn("dbo.Ecps", "email", c => c.String());
        }
    }
}
