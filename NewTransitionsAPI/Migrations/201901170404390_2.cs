namespace NewTransitionsAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuizAnswers", "eyeglasses_frequency", c => c.String());
            AddColumn("dbo.QuizAnswers", "gender", c => c.String());
            AddColumn("dbo.QuizAnswers", "age_group", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuizAnswers", "age_group");
            DropColumn("dbo.QuizAnswers", "gender");
            DropColumn("dbo.QuizAnswers", "eyeglasses_frequency");
        }
    }
}
