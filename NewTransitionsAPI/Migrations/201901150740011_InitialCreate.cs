namespace NewTransitionsAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ecps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        contactFullName = c.String(),
                        email = c.String(),
                        password = c.String(),
                        businessName = c.String(),
                        street = c.String(),
                        zipcode = c.String(),
                        state = c.String(),
                        city = c.String(),
                        phoneNumber = c.String(),
                        userID = c.Guid(nullable: false),
                        token = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuizAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Answer1 = c.Boolean(nullable: false),
                        Answer2 = c.Boolean(nullable: false),
                        Answer3 = c.Boolean(nullable: false),
                        Answer4 = c.Boolean(nullable: false),
                        Answer5 = c.Boolean(nullable: false),
                        Answer6 = c.Boolean(nullable: false),
                        ECPId = c.String(),
                        ResultId = c.Guid(nullable: false),
                        Result = c.String(),
                        DateCreated = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.QuizAnswers");
            DropTable("dbo.Ecps");
        }
    }
}
